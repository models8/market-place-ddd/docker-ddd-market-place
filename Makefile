clones:
	git clone git@gitlab.com:models8/market-place-ddd/docker-ddd-market-place.git
volume: 
	docker volume create event-store-db
install:
	docker-compose -f ./docker-compose.builder.yml run --rm install-market-place
	
#--------------Init and Reset------------------ 
init-dev:
	make volume
	make install
reset-dev:
	docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml down -v --rmi "local"
	docker volume rm event-store-db

#--------------Up and Down------------------ 
up-dev: 
	docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml up 
down-dev:
	docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml down -v --rmi "local"	


#-------------Start and Stop------------------ 
	
start-dev:
	docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml start
stop-dev:
	docker-compose -f ./docker-compose.yml -f ./docker-compose.dev.yml stop




