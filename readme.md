# DEV ENV docker >= 3.4 : 
- make dev env from project
```sh
$ make clones
$ make init-dev
$ make up-dev
```

- remove all images, volumes, containers created by "make dev"
```sh 
$ make reset-dev
```

- Other commands
```sh 
$ make stop-dev
```
```sh 
$ make start-dev
```

# ENV VARIABLES


# ENV FILE EXEMPLE : 


